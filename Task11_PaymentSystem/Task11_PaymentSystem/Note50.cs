﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSystem
{
    class Note50 : ICash
    {

        int amount = 0;
        int value = 50;
        public int Amount { get => amount; set => amount = value; }

        public int Value => value;

        public Note50(int amount)
        {
            Amount = amount;
        }

    }
}
