﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSystem
{
    class CreditCard : ICard
    {
        private float balance;
        private float credit;

        public float Balance { get => balance; set => balance = value; }
        public float Credit { get => credit; set => credit = value; }

        public CreditCard(float balance)
        {
            Balance = balance;
            Credit = 0;
        }

        public CreditCard(float balance, float credit)
        {
            Balance = balance;
            Credit = credit;
        }

        public bool Pay(float amount)
        {
            Balance -= amount;
            if(Balance < 0)
            {
                Credit += Math.Abs(Balance);
                Balance = 0;
            }
            return true;
        }

        public override string ToString()
        {
            return $"There is now {Balance} money left on the card and there is {Credit} money in credit";
        }
    }
}
