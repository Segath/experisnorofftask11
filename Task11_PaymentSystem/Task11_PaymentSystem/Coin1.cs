﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSystem
{
    class Coin1 : ICash
    {

        int amount = 0;
        int value = 1;
        public int Amount { get => amount; set => amount = value; }

        public int Value => value;

        public Coin1(int amount)
        {
            Amount = amount;
        }

    }
}
