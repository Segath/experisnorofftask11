﻿using System;
using System.Collections.Generic;

namespace Task11_PaymentSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("How much did it cost? ");
            int cost = int.Parse(Console.ReadLine());

            // How much money there are on the cards
            int money = 100;

            Customer cust = new Customer();


            Console.Write("Pay with cash(1) or card(2)? ");
            if (Console.ReadLine() == "1")
            {
                // Add wallet with money to the customer
                Dictionary<string, ICash> wallet = new Dictionary<string, ICash>
                {
                    { "1", new Coin1(10) },
                    { "5", new Coin5(5) },
                    { "10", new Coin10(2) },
                    { "50", new Note50(2) },
                    { "100", new Note100(1) }
                };
                cust.Wallet = wallet;

                PayWithCash(cust, cost);
            }
            else
            {
                Console.Write("Saving(1) or credit(2) card? ");
                if(Console.ReadLine() == "1")
                {
                    cust.Card = new SavingsCard(money);
                    if (!cust.Card.Pay(cost))
                    {
                        Console.WriteLine("Not enough money on card");
                    }
                    else
                    {
                        Console.WriteLine(cust.Card.ToString());
                    }
                }
                else
                {
                    cust.Card = new CreditCard(money);
                    cust.Card.Pay(cost);
                    Console.WriteLine(cust.Card.ToString());
                }
            }
        }

        public static Customer PayWithCash(Customer cust, int cost)
        {
            // Temporary value in case there is not enough money in the wallet
            Customer tempCust = cust;

            // Pay
            while (cost > 0)
            {
                if(cust.Wallet["100"].Amount > 0)
                {
                    cost -= cust.Wallet["100"].Value;
                    cust.Wallet["100"].Amount--;
                }
                else if(cust.Wallet["50"].Amount > 0)
                {
                    cost -= cust.Wallet["50"].Value;
                    cust.Wallet["50"].Amount--;
                }
                else if (cust.Wallet["10"].Amount > 0)
                {
                    cost -= cust.Wallet["10"].Value;
                    cust.Wallet["10"].Amount--;
                }
                else if (cust.Wallet["5"].Amount > 0)
                {
                    cost -= cust.Wallet["5"].Value;
                    cust.Wallet["5"].Amount--;
                }
                else if (cust.Wallet["1"].Amount > 0)
                {
                    cost -= cust.Wallet["1"].Value;
                    cust.Wallet["1"].Amount--;
                }
                else
                {
                    Console.WriteLine("Not enough money");
                    return tempCust;
                }
            }

            int diff = Math.Abs(cost);
            Console.WriteLine("Change left after transaction is: " + diff);
            // Change
            while(diff > 0)
            {
                if(diff >= 100)
                {
                    cust.Wallet["100"].Amount++;
                    diff -= 100;
                    Console.WriteLine("Received change in form of: 100 note");
                }
                else if (diff >= 50)
                {
                    cust.Wallet["50"].Amount++;
                    diff -= 50;
                    Console.WriteLine("Received change in form of: 50 note");
                }
                else if (diff >= 10)
                {
                    cust.Wallet["10"].Amount++;
                    diff -= 10;
                    Console.WriteLine("Received change in form of: 10 coin");
                }
                else if (diff >= 5)
                {
                    cust.Wallet["5"].Amount++;
                    diff -= 5;
                    Console.WriteLine("Received change in form of: 5 coin");
                }
                else if (diff >= 1)
                {
                    cust.Wallet["1"].Amount++;
                    diff -= 1;
                    Console.WriteLine("Received change in form of: 1 coin");
                }
            }

            // Return the customer after payment to reflect the changes in the wallet
            return cust;
        }
    }
}
