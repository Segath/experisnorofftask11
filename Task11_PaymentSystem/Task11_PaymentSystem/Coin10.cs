﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSystem
{
    class Coin10 : ICash
    {
        int amount = 0;
        int value = 10;
        public int Amount { get => amount; set => amount = value; }

        public int Value => value;

        public Coin10(int amount)
        {
            Amount = amount;
        }
    }
}
