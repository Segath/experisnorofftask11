﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSystem
{
    class Customer
    {
        private Dictionary<string, ICash> wallet;
        private ICard card;

        public ICard Card { get => card; set => card = value; }

        public Dictionary<string, ICash> Wallet { get => wallet; set => wallet = value; }

        public Customer(Dictionary<string, ICash> wallet = null, ICard card = null)
        {
            Wallet = wallet;
            Card = card;
        }
    }
}
