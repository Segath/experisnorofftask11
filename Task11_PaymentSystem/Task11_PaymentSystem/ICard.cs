﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSystem
{
    interface ICard
    {

        float Balance { get; set; }
        bool Pay(float amount);
    }
}
