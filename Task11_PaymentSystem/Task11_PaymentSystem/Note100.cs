﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSystem
{
    class Note100 : ICash
    {

        int amount = 0;
        int value = 100;
        public int Amount { get => amount; set => amount = value; }

        public int Value => value;

        public Note100(int amount)
        {
            Amount = amount;
        }

    }
}
