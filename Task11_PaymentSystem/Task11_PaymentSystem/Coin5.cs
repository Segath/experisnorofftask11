﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSystem
{
    class Coin5 : ICash
    {

        int amount = 0;
        int value = 5;
        public int Amount { get => amount; set => amount = value; }

        public int Value => value;

        public Coin5(int amount)
        {
            Amount = amount;
        }

    }
}
