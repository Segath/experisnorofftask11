﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSystem
{
    interface ICash
    {
        int Value { get; }
        int Amount { get; set; }
    }
}
