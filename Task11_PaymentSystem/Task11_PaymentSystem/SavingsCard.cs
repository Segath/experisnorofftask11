﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_PaymentSystem
{
    class SavingsCard : ICard
    {
        private float balance;

        public float Balance { get => balance; set => balance = value; }

        public SavingsCard(float balance)
        {
            Balance = balance;
        }

        public bool Pay(float amount)
        {
            if(Balance - amount >= 0)
            {
                Balance -= amount;
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            return $"There is now {Balance} money left on the card";
        }
    }
}
